# 每日英語跟讀 Ep.K190: About US - 拜登的維修權命令是個好消息與美國尼特族 
##  每日英語跟讀 Ep.K190: About US - Biden’s right-to-repair order is good news for people who always break their phones

After years of **petitions** and proposals, **momentum** is building in government to give consumers **broader** rights to repair products they own ranging from farm **tractors** to mobile phones.

The right-to-repair issue has brought farmers in league with electronic and computer **enthusiasts**, who for years have **groused** at the thought that they **merely** licensed software, didn’t own it, and weren’t able to modify it to their liking.

In a press briefing yesterday, White House Press Secretary Jen Psaki said President Joe Biden will issue an executive order directing the US Department of **Agriculture** to issue rules that "give farmers the right to repair their own equipment how they like."

Biden will **urge** the **Federal Trade Commission** to press computer and electronics **manufacturers** and defense **contractors** to offer additional **leeway** in how their devices are repaired.
___
在多年的請願與提案後，政府正在推動給予消費者更大的權利去維修他們擁有的產品，範疇從農場的曳引機到手機。

維修權問題讓農民與電子產品及電腦的愛好者結盟，他們多年來一直抱怨自己只獲授權使用軟體，並未擁有它，也無法根據自身喜好調整它。

在昨天的新聞發布會中，白宮新聞秘書珍．莎琪表示，拜登總統將發布一項行政命令，指示美國農業部發布規定，「賦予農民權利，隨他們的喜好修復自己擁有的設備」。

拜登還將敦促聯邦貿易委員會施壓電腦、電子產品製造商與國防承包商，在如何修復他們的裝置上提供更多餘地。


## One in five young adults is neither working nor studying in US 美國5名年輕成人就有1人沒在工作，也沒就學

Almost one in five young adults in the U.S. was neither working nor studying in the first quarter as Black and **Hispanic** youth remain **idle** at **disproportionate** rates.

In the first three months of the year, about 3.8 million Americans age 20 to 24 were not in employment, education or training, known as the **NEET** rate, the Center for Economic Policy and Research said in a report. That’s up by 740,000, or 24%, from a year earlier.

Inactive youth is a worrying sign for the future of the economy. Further, high NEET rates may **foster** environments that are **fertile** for social unrest.
___

今年第1季美國幾乎每5名年輕成年人中，就有1人沒在工作也沒在就學，非裔與西裔年輕人賦閒率更是不成比例。

「經濟政策與研究中心」在一份報告中說，今年前3個月，約380萬名20歲到24歲的人處於非就業、就學或接受培訓，即所謂「尼特」率，比一年前多出74萬人，或者24％。

年輕人閒閒沒事做，是經濟前景一個令人擔心的徵兆。而且，高「啃老」率或許助長醞釀社會不安的環境。
